projection(cut=false) a();

projection(cut=true)    union() {
        b(-23);
        b();
        b(24);
}

module b(translationX=0) {
    translate([translationX, -1, -1]) cube([8, 5, 4]);
}

module a() {
        rotate([90, 0, 0]) 
        translate([0, 15, 0]) 
            import("Xbox_Wall_Mount_V.07.stl", convexity=3);
}