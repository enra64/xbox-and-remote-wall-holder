smallerCircleDiameter = 6.8;
largerCircleDiameter = 12;
slotLength = 14;
smallerCircleRadius = smallerCircleDiameter / 2;
largerCircleRadius = largerCircleDiameter / 2;

union() {
    
circle(r=smallerCircleRadius, $fn=100);
translate([0, -slotLength, 0]) circle(r=largerCircleRadius, $fn=100);
translate([-smallerCircleRadius, -slotLength, 0])
    square([smallerCircleDiameter,slotLength]);
};