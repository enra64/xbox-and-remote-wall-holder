# Xbox_Wall_Mount_V.07.stl
Created by Joel Pratt and licensed as Creative Commons, from https://www.thingiverse.com/thing:3769609.

# Hisense Logo
The Hisense logo is property of Hisense, taken from hisense.com via http://www.freelogovectors.net/hisense-logo/.

# Xbox Logo
The Xbox logo is probably property of Microsoft, via https://upload.wikimedia.org/wikipedia/commons/f/f9/Xbox_one_logo.svg.

# Creative Commons!
At least for the stuff I did. I'd appreciate a reference, though ;)
